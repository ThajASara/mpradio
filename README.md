# mpradio
---------

This is a small script I am working on that remembers the internet radio stations I enjoy listening to.  It presents the user with a list of stations to choose from and then plays the selection in mplayer. 
It requires the mpradio.config file in order to have a list of stations to play. You can place this file wherever you like, but you will need to change the loctaion in the script to match. 