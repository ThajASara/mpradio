#!/bin/bash


# This is a small script I made to remember internet radio streams that I like and play the
# in mplayer.
#
# Copyright (C) 2018  Thaj A. Sara
#
# This program is free software: you can redistribute it and/or modify it under the terms of
# the GNU General Public License as published by the Free Software Foundation, either version
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
# without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with this program.
# If not, see <https://www.gnu.org/licenses/>.

source ~/mpradio.config #Change the location at this line to match where you have placed you mpradio.config file. 
echo ""
printf '%s\n' "${STATION[@]}"
echo ""
echo "Choose a station from the list above."
read CHOICE

#Check to see if user input is valid 

LENGTH=${#STATION[@]}

if [ "$CHOICE" -le "$LENGTH" ]
then
    echo 'Playing selection'
else
    echo 'INVALID SELECTION: Please choose a selection by number from the list above.'
    exit
fi

#Command to send selection to mplayer     

mplayer ${URL[$CHOICE]}
